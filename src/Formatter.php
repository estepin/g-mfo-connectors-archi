<?php
namespace MfoRu\MfoAccounting\Archi;

use MfoRu\Contracts\MfoAccounting\Anket;

class Formatter
{
    function passSeriaNum(Anket $anket)
    {
        $ex = explode(' ', $anket->passSeriaNum);
        $pass['seria'] = implode(' ', [$ex[0], $ex[1]]);
        $pass['num'] = $ex[2];

        return $pass;
    }

    function anketToClientArray(Anket $anket, $clientApi)
    {
        $rusId = $this->findRussiaIdFromDict($clientApi);
        $pass = $this->passSeriaNum($anket);

        $addressReg = [
            'country' => $rusId,
            'postindex' => $anket->regIndex,
            'statestr' => $anket->regRegion,
            'locality' => $anket->regCity,
            'street' => $anket->regStreet,
            'house' => $anket->regHouse,
            'build2' => $anket->regBuild,
            'noflat' => $anket->regRoom
        ];

        $addressFact = [
            'country' => $rusId,
            'postindex' => $anket->liveIndex,
            'statestr' => $anket->liveRegion,
            'locality' => $anket->liveCity,
            'street' => $anket->liveStreet,
            'house' => $anket->liveHouse,
            'build2' => $anket->liveBuild,
            'noflat' => $anket->liveRoom
        ];

        return [
            'sname' => $anket->lastname,
            'fname' => $anket->firstname,
            'mname' => $anket->middlename,
            'birthdate' => $this->formatDate($anket->birthdate),
            'birthplace' => $anket->passBirthPlace,
            'docs' => $pass['seria'],
            'docnum' => $pass['num'],
            'docbegindate' => $this->formatDate($anket->passDate),
            'doccode' => $anket->passCode,
            'doccontent' => $anket->passWhoIssued,
            'mobilephone' => $anket->phone,
            'email' => $anket->email,
            'addressreg' => $addressReg,
            'addressfact' => $addressFact
        ];
    }

    function anketToAnketArray(Anket $anket, $clientId, $relisePlaceId, $poid)
    {
        return [
            'clientid' => $clientId,
            'reliseplaceid' => $relisePlaceId,
            'poid' => $poid,
            'loancostall' => $anket->summ,
            'daysquant' => $anket->term
        ];
    }

    function findRussiaIdFromDict($client)
    {
        $data = $client->getDictionary('COUNTRYES');
        $dictionary = $data->dictionary;
        $rfSynonyms = [
            'Россия',
            'Российская Федерация',
            'РФ'
        ];

        foreach ($rfSynonyms as $name)
        {
            if($r = $this->findInDict($dictionary, 'name', $name))
            {
                return $r->id;
            }
        }

        throw new \Exception('Russia not found in dictionary');
    }

    protected function formatDate($date)
    {
        return date('d.m.Y', strtotime($date));
    }

    protected function findInDict($dictData, $propName, $value)
    {
        if(is_string($value))
            $value = mb_strtolower($value);

        foreach ($dictData as $item)
        {
            if(is_string($value))
            {
                if(mb_strtolower($item->$propName) == $value)
                {
                    return $item;
                }
            }
            else
            {
                if($item->$propName == $value)
                {
                    return $item;
                }
            }
        }

        return false;
    }
}