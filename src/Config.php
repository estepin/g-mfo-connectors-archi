<?php
namespace MfoRu\MfoAccounting\Archi;

class Config
{
    public $uri;
    public $secret;
    public $defaultRelisePlaceId;
    public $defaultPoidId;

    public $updateClientIfExists;

}