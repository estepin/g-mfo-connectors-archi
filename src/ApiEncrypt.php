<?php
namespace MfoRu\MfoAccounting\Archi;

class ApiEncrypt
{
    protected $secret;

    function __construct($secret)
    {
        $this->secret = $secret;
    }

    protected function encrypt($fields, $sequence, $funcName)
    {
        $secretParams = $this->encryptSequence($fields, $sequence);
        return $this->getCheckSum($funcName, $secretParams);
    }

    function encryptSequence($fields, $sequence)
    {
        $secretParams = [];
        foreach ($sequence as $propName)
        {
            if (isset($fields[$propName]))
            {
                if($propName == 'addressreg' || $propName == 'addressfact')
                {
                    $seq = $this->getSequenceAddress();
                    $s = $this->encryptSequence((array)$fields[$propName], $seq);
                    $secretParams = array_merge($secretParams, $s);
                }
                else
                {
                    $secretParams[] = $fields[$propName];
                }
            }
        }

        return $secretParams;
    }

    function encryptClientInfo($fields)
    {
        $sequence = $this->getSequenceClientInfo();
        return $this->encrypt($fields, $sequence, 'getclientinfo');
    }

    function encryptClient($fields)
    {
        $sequence = $this->getSequenceClient();
        return $this->encrypt($fields, $sequence, 'createclient');
    }

    function encryptClientUpdate($fields)
    {
        $sequence = $this->getSequenceClient();
        return $this->encrypt($fields, $sequence, 'updateclient');
    }

    function encryptOrder($fields)
    {
        $sequence = $this->getSequenceOrder();
        return $this->encrypt($fields, $sequence, 'createorder');
    }


    protected function getCheckSum($func, $params)
    {
        return md5($func.implode('', $params).$this->secret);
    }

    protected function getSequenceAddress()
    {
        return [
            'country', 'postindex', 'state', 'statestr', 'zone', 'localitytype',
            'locality', 'streettype', 'street', 'build', 'build2', 'house', 'noflat',
            'kladr'
        ];
    }

    protected function getSequenceClient()
    {
        return [
            'sname', 'fname', 'mname', 'birthdate', 'birthplace', 'familystatus', 'familyquant', 'docs',
            'docnum', 'docbegindate', 'doccode', 'doccontent', 'phone', 'mobilephone', 'email',
            'smssend', 'realestateexist', 'autoexist', 'addressequal', 'resstat', 'education',
            'docdatereg', 'addressreg', 'addressfact', 'photo', 'passportfile', 'passportfilename',
            'addressfile', 'addressfilename', 'reliseplaceid', 'userid', 'reason', 'consent',
            'consentdate', 'consentenddate', 'rsacct', 'koracct', 'bank', 'inn', 'kpp', 'okato',
            'bik', 'cardnumber', 'snils'
        ];
    }

    protected function getSequenceClientUpdate()
    {
        return [
            'role', 'sname', 'fname', 'mname', 'birthdate', 'birthplace', 'familystatus', 'familyquant', 'docs',
            'docnum', 'docbegindate', 'doccode', 'doccontent', 'phone', 'mobilephone', 'email',
            'smssend', 'realestateexist', 'autoexist', 'addressequal', 'resstat', 'education',
            'docdatereg', 'addressreg', 'addressfact', 'photo', 'passportfile', 'passportfilename',
            'addressfile', 'addressfilename', 'reliseplaceid', 'userid', 'reason', 'consent',
            'consentdate', 'consentenddate', 'rsacct', 'koracct', 'bank', 'inn', 'kpp', 'okato',
            'bik', 'cardnumber', 'snils'
        ];
    }

    protected function getSequenceClientInfo()
    {
        return [
            'orderid', 'ordernumber', 'clientid', 'clientname', 'clientdocs', 'clientdocn', 'poid',
            'daysquant', 'costall', 'rpid', 'orgid', 'name', 'userid', 'password', 'ref', 'startdate',
            'enddate', 'all'
        ];
    }

    protected function getSequenceOrder()
    {
        return [
            'clientid', 'reliseplaceid', 'poid', 'loancostall', 'daysquant', 'loanaim', 'informationsource',
            'monthincomenottax', 'workname', 'workaddress', 'workprof', 'workstazh', 'workphone',
            'workheadmen', 'occupation', 'occupationtext', 'suppressflag', 'additionalincome', 'oldcredits',
            'recomend', 'friend', 'childrenbefore21', 'izhdiveneclist', 'supruginfo', 'adultinfo',
            'info', 'userid', 'status', 'statuschangeinfo', 'cancelstatus'
        ];
    }

}