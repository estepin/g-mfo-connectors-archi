<?php
namespace MfoRu\MfoAccounting\Archi;

use GuzzleHttp\Exception\ConnectException;
use MfoRu\Contracts\MfoAccounting\Exceptions\ServerUnavailable;
use MfoRu\MfoAccounting\Archi\Exceptions\CheckSumException;

class Api
{
    protected $client;
    protected $url;
    protected $secret;

    protected $encrypter;

    protected $lastRequest;
    protected $lastResponse;

    function __construct($url, $secret)
    {
        $this->url = $url;
        $this->secret = $secret;
        $this->client = new \GuzzleHttp\Client(['timeout' => 15]);
        $this->encrypter = new ApiEncrypt($secret);
    }

    function createOrder($orderData)
    {
        $secret = $this->encrypter->encryptOrder($orderData);
        $data = [
            'command' => 'createorder',
            'createorder' => $orderData,
            'check' => $secret
        ];

        return $this->request($data);
    }

    function getDictionary($name)
    {
        $secret = $this->getCheckSum('getdictionary', [$name]);
        $postData = [
            'json' => [
                'command' => 'getdictionary',
                'value' => [
                    'name' => $name,
                ],
                'check' => $secret
            ]
        ];

        $response = $this->client->request(
            'POST',
            $this->url,
            $postData
        );

        $body = $response->getBody();
        return \GuzzleHttp\json_decode($body);
    }

    function updateClient($clientData)
    {
        $secret = $this->encrypter->encryptClientUpdate($clientData);

        $data = [
            'command' => 'updateclient',
            'updateclient' => $clientData,
            'check' => $secret
        ];

        return $this->request($data);
    }

    function getClientInfo($valueArr)
    {
        $secret = $this->encrypter->encryptClientInfo($valueArr);

        $data = [
            'command' => 'getclientinfo',
            'value' => $valueArr,
            'check' => $secret
        ];

        return $this->request($data);
    }

    function createClient($clientData)
    {
        $secret = $this->encrypter->encryptClient($clientData);

        $data = [
            'command' => 'createclient',
            'createclient' => $clientData,
            'check' => $secret
        ];
        return $this->request($data);
    }

    protected function getCheckSum($func, $params)
    {
        return md5($func.implode('', $params).$this->secret);
    }

    function getLastRequest()
    {
        return $this->lastRequest;
    }

    function getLastResponse()
    {
        return $this->lastResponse;
    }

    protected function request($data)
    {
        $postData = [
            'json' => $data
        ];

        $this->lastRequest = \GuzzleHttp\json_encode($data);

        try {
            $response = $this->client->request(
                'POST',
                $this->url,
                $postData
            );
        }catch (ConnectException $e) {
            throw new ServerUnavailable();
        }


        $body = $response->getBody();
        $this->lastResponse = $response = $body->getContents();

        $respData = \GuzzleHttp\json_decode($response);

        if(isset($respData->errorcode) && $respData->errorcode == 99)
        {
            throw new CheckSumException();
        }

        return $respData;
    }
}