<?php
namespace MfoRu\MfoAccounting\Archi;

use MfoRu\Contracts\MfoAccounting\Anket;
use MfoRu\Contracts\MfoAccounting\Connector as BaseInterface;
use MfoRu\Contracts\MfoAccounting\DebugData as DebugDataInterface;
use MfoRu\Contracts\MfoAccounting\Exceptions\UnknownResponse;
use MfoRu\Contracts\MfoAccounting\Exceptions\WrongConfig;
use MfoRu\MfoAccounting\Archi\Exceptions\CheckSumException;

class Connector implements BaseInterface
{
    protected $formatter;

    function __construct()
    {
        $this->formatter = new Formatter();
    }

    function addToUpload(Anket $anket, $config)
    {
        if(!$this->validateConfig($config))
            throw new WrongConfig();

        $api = new Api($config->uri, $config->secret);

        if(!$clientId = $this->clientIdFromAnket($anket, $api))
            $clientId = $this->createClientFromAnket($anket, $api);

        $orderId = $this->sendAnket($anket, $api, $clientId, $config->defaultRelisePlaceId, $config->defaultPoidId);
        if(!$orderId)
            throw new \Exception();

        return $orderId;
    }

    protected function sendAnket(Anket $anket, $api, $clientId, $relisePlaceId, $poid)
    {
        $anketArr = $this->formatter->anketToAnketArray($anket, $clientId, $relisePlaceId, $poid);
        $result = $api->createOrder($anketArr);

        if(!isset($result->refid))
            throw new UnknownResponse('Can\'t create order Archi');

        return $result->refid;
    }

    protected function clientIdFromAnket(Anket $anket, $clientApi)
    {
        $pass = $this->formatter->passSeriaNum($anket);
        $clientData = $clientApi->getClientInfo(['clientdocs' => $pass['seria'], 'clientdocn' => $pass['num']]);

        if(isset($clientData->errorcode) && $clientData->errorcode === 199)
            return false;
        elseif ($clientData->id)
            return $clientData->id;
        else
            throw new UnknownResponse();

    }

    protected function createClientFromAnket(Anket $anket, $clientApi)
    {
        $newClientArr = $this->formatter->anketToClientArray($anket, $clientApi);
        $result = $clientApi->createClient($newClientArr);

        if(!isset($result->refid))
            throw new UnknownResponse('Can\'t create client Archi');

        return $result->refid;
    }

    function checkStatus($anketId, $config)
    {
        throw new \Exception('This method is not implemented');
    }

    function getConfigModel()
    {
        return new Config();
    }

    function getAccData($id)
    {
        return false;
    }

    function testConfig($config): bool
    {
        $anket = $this->getTestAnket();
        try{
            return (bool)$this->addToUpload($anket, $config);
        }catch (CheckSumException $e){
            throw new WrongConfig();
        }
    }

    function getTestAnket()
    {
        $anket = new Anket();

        $anket->summ = '1000';
        $anket->term = '5';

        $anket->lastname = 'Петров';
        $anket->firstname = 'Петр';
        $anket->middlename = 'Петрович';
        $anket->gender = '1';
        $anket->birthdate = '1990-01-01';

        $anket->email = 'test@mfo.ru';
        $anket->phone = '+79091112233';

        $anket->passSeriaNum = '11 26 123321';
        $anket->passWhoIssued = 'УВД';
        $anket->passCode = '123-321';
        $anket->passDate = '2011-01-01';
        $anket->passBirthPlace = 'Саратов';

        $anket->regIndex = '410001';
        $anket->regRegion = 'Саратовская обл';
        $anket->regCity = 'Саратов';
        $anket->regStreet = 'Московская';
        $anket->regHouse = '1';
        $anket->regRoom = 1;

        $anket->liveIndex = '410001';
        $anket->liveRegion = 'Саратовская обл';
        $anket->liveCity = 'Саратов';
        $anket->liveStreet = 'Московская';
        $anket->liveHouse = '1';
        $anket->liveRoom = 1;

        return $anket;
    }

    protected function validateConfig($config)
    {
        $essentialFields = [
            'uri',
            'defaultRelisePlaceId',
            'defaultPoidId'
        ];

        foreach($essentialFields as $f)
        {
            if(!isset($config->$f) || empty($config->$f))
                return false;

        }

        return true;
    }

    function getDebugData():DebugDataInterface
    {
        return new DebugData();
    }
}